import sys

import PySide
from PySide.QtGui import QApplication
from PySide.QtGui import QMessageBox


import ftrack_api
import os
import requests.packages.urllib3
requests.packages.urllib3.disable_warnings()
app = None

session = ftrack_api.Session(
    server_url='https://XXX.ftrackapp.com',
    api_user='XXXX',
    api_key='XXXXX-XXXX-XXXX-XXXX-XXXXXXX',
    auto_connect_event_hub=False)

# print session.types.items()
# users = session.types['User']
# print users
# print user.keys()

taskid = os.environ['FTRACK_TASKID']
shotid = os.environ['FTRACK_SHOTID']

fstart = os.environ['FS']
fend = os.environ['FE']

tasks = session.query(
    'Task where id is {}'.format(taskid)
)
print "INFO:"
print "\nTasks:"
for task in tasks:
    print task['name']

shots = session.query(
    'Shot where id is {}'.format(shotid)
)

# test = session.query('Shot')
try:
    projectName = shots[0]['_link'][0]['name']
except Exception:
    projectName = "Assets"
    
try:
    projectId = shots[0]['_link'][0]['id']
except Exception:
    projectId = '0'
    
print 'project: ' + projectName + ' project ID: ' + projectId

query = session.query(
    'Project where id is {}'.format(projectId)
)
metadata = query[0]['metadata']
resolution = metadata['resolution']
resolution = resolution.split()
resolution_x = resolution[0]
resolution_y = resolution[1]
print resolution_y


print "\nShot:"
for shot in shots:
    print shot['name']

print 'Frame Start: ' + fstart
print 'Frame End: ' + fend

# print shot['custom_attributes'].items()
fps = shot['custom_attributes']['fps']
print 'Fps: ' + str(fps)

fstart = int(fstart)
fend = int(fend)
fps = float(fps)

ix.cmds.SetFps(fps)
ix.cmds.SetCurrentFrameRange(fstart, fend)
ix.cmds.SetValues(["project://scene/image.resolution[0]"], [str(resolution_x)])
ix.cmds.SetValues(["project://scene/image.resolution[1]"], [str(resolution_y)])
ix.cmds.SetValues(["project://scene/image.resolution_multiplier"], ["1"])




# Create the application object
app = None
if not QApplication.instance(): #QtApplication doesn't exist
    app = QApplication(sys.argv)
else:
    app = QApplication.instance()
 
# Create a simple dialog box
msgBox = QMessageBox()
msgBox.setText("Project " + projectName)
msgBox.exec_()